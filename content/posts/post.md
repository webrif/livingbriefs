---
title: "Hello from 2023"
date: 2023-04-12T17:23:46Z
draft: false
---

# Hello
Here I'm going to add some usefull scripts
```bash
#!/bin/bash
export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get install -y docker-ce
systemctl enable --now docker
usermod -aG docker ubuntu
apt-get install -y awscli
aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 858610611550.dkr.ecr.us-east-2.amazonaws.com/forecast
docker pull 858610611550.dkr.ecr.us-east-2.amazonaws.com/forecast:latest
docker run -d 858610611550.dkr.ecr.us-east-2.amazonaws.com/forecast:latest
```
